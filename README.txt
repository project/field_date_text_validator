CONTENTS OF THIS FILE
---------------------
   
 * SUMMARY
 * Installation
 * Configuration
 * Maintainers

-- SUMMARY --

This small module is to validate a field with Hijri inputs, using a widget for
a text field.

-- INSTALLATION --

* Install as usual, see:
https://www.drupal.org/documentation/install/modules-themes/modules-7
for further information.


-- CONFIGURATION --

 - Edit the field you wish and change (Widget type)->Hijri Validator, choose
 your settings for the field(Hijri Validator) You'll have the option to select
 the century you want the module to allow(14xx Ex:1438), (13xx Ex:1338) or both.

-- Maintainers --
Current maintainers:
* Essam AlQaie (3ssom) - https://www.drupal.org/u/3ssom
* Saud Alfadhli (samaphp) - https://www.drupal.org/u/samaphp
